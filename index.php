
<html>
<head>
<style>
  #wrapper{
  	width:960px;
  	margin:0px auto;
  }
</style>
</head>
<body>
<div id="wrapper">
<h2> /***********Printing the numbers from 1 to 100. **********/</h2>
<?php
  $i=0; 																		//Declaration of variable for looping upto 100
   for($i=1;$i<=100;$i++){  													// for loop to loop over $i upto 100
   		switch($i){  															// Switch case to switch on different $i values
   			case ($i%3==0 && $i%5==0)	: 	echo "<b>FizzBuzz</b>";     			// case  to check if multiple of 3 and 5
   											/***Condition to break line after 10 digits **/ 	
   												if($i%10==0){
   													echo "<br/>";
   												}else{
   													echo ", ";
   												}
   											break;
   			case ($i%3==0)				: 	echo "<b>Fizz</b>";   					// case to check if multiple of 3
   												/***Condition to break line after 10 digits **/
   												if($i%10==0){
   														echo "<br/>";
   												}else{
   													echo ", ";
   												}
   											break;  	
   			case ($i%5==0)				: 	echo "<b>Buzz</b>";     				// case to check if multiple of 5
			   									/***Condition to break line after 10 digits **/
			   									if($i%10==0){
			   										echo "<br/>";
			   									}else{
   													echo ", ";
   												}
			   								break;
   			default 					:	echo $i;					 // default case to print $i value
			   									/***Condition to break line after 10 digits **/
			   									if($i%10==0){
			   										echo "<br/>";
			   									}else{
   													echo ", ";
   												}
   		}
   }


?>
</div>

</body>
</html>